package com.example.demo.api;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import org.springframework.boot.web.client.RestTemplateBuilder;

import java.io.IOException;

public class RestUtil {
    public static OkHttpClient client = new OkHttpClient();
    private static String API_KEY = "";
    public static String retrieveSenators() {
        String result = null;
        Request request = new Request.Builder()
                .header("X-API-Key", API_KEY)
                .url("https://api.propublica.org/congress/v1/116/senate/members.json")
                .build();
        try {
            Response response = client.newCall(request).execute();
            result = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
    return result;
    }

    public static void getMostPopularCelebrities() {
        Request request = new Request.Builder()
                .url("https://imdb8.p.rapidapi.com/actors/list-most-popular-celebs?currentCountry=US&purchaseCountry=US&homeCountry=US")
                .get()
                .addHeader("x-rapidapi-host", "imdb8.p.rapidapi.com")
                .addHeader("x-rapidapi-key", "de465dc97dmsh56e626c262399b3p1b4411jsne62cec4caf3e")
                .build();

        try {
            Response response = client.newCall(request).execute();
            System.out.println(response.body().string());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static String retrieveCongress() {
        String result = null;
        Request request = new Request.Builder()
                .header("X-API-Key", API_KEY)
                .url("https://api.propublica.org/congress/v1/116/house/members.json")
                .build();
        try {
            Response response = client.newCall(request).execute();
            result = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

}