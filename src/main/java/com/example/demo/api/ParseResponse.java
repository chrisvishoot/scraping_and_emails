package com.example.demo.api;

import com.example.demo.model.Member;
import com.example.demo.model.Result;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.List;

public class ParseResponse {
    private static JSONParser parser = new JSONParser();

    public static List<Member> getResult(String initialResponse) throws ParseException {
        JSONObject initial = (JSONObject) parser.parse(initialResponse);
        String resultString  = initial.get("results").toString();
        JSONArray resultParse = (JSONArray) parser.parse(resultString);
        List<Member> allMembers = new ArrayList<>();
        for(Object object : resultParse) {
            JSONObject resultObj = (JSONObject) object;
            JSONArray members = (JSONArray) resultObj.get("members");
            allMembers.addAll(getMembers(members));
        }
        return allMembers;
    }
    public static List<Member> getMembers(JSONArray membersJSONArray) {
        List<Member> members = new ArrayList<>();
        for(Object object : membersJSONArray) {
            JSONObject json = (JSONObject) object;
            String title = null;
            if(json.get("title") != null) {
                title = json.get("title").toString();
            }
            String firstName = null;
            if(json.get("first_name") != null) {
                firstName = json.get("first_name").toString();
            }
            String lastName = null;
            if(json.get("last_name") != null) {
                lastName = json.get("last_name").toString();
            }
            String url = null;
            if(json.get("url") != null) {
                url = json.get("url").toString();
            }
            String contact_form = null;
            if(json.get("contact_form") != null) {
                contact_form = json.get("contact_form").toString();
            }
            String facebook = null;
            if(json.get("facebook_account") != null) {
                facebook = json.get("facebook_account").toString();
            }
            String youtube = null;
            if(json.get("youtube_account") != null) {
                youtube = json.get("youtube_account").toString();
            }
            String twitter = null;
            if(json.get("twitter_account") != null) {
                twitter = json.get("twitter_account").toString();
            }
            String phoneNumber = null;
            if(json.get("phone") != null) {
                phoneNumber = json.get("phone").toString();
            }
            String office = null;
            if(json.get("office") != null) {
                office = json.get("office").toString();
            }
            Member member = new Member(title, firstName, lastName, url, contact_form, facebook, youtube, twitter,
                    phoneNumber, office);
            members.add(member);
        }
        return members;
    }


}
