package com.example.demo.model;

import java.util.List;

public class Result {
    private String congress;
    private String chamber;
    private long num_results;
    private long offset;
    private List<Member> members;

    @Override
    public String toString() {
        return "Result{" +
                "congress='" + congress + '\'' +
                ", chamber='" + chamber + '\'' +
                ", num_results=" + num_results +
                ", offset=" + offset +
                '}';
    }

    public String getCongress() {
        return congress;
    }

    public void setCongress(String congress) {
        this.congress = congress;
    }

    public String getChamber() {
        return chamber;
    }

    public void setChamber(String chamber) {
        this.chamber = chamber;
    }

    public long getNum_results() {
        return num_results;
    }

    public void setNum_results(long num_results) {
        this.num_results = num_results;
    }

    public long getOffset() {
        return offset;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }
}
