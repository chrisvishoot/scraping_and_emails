package com.example.demo.model;

public class Member {
    private String title;
    private String first_name;
    private String last_name;
    private String url;
    private String contact_form;
    private String facebook;
    private String youtube;
    private String twitter;
    private String phoneNumber;
    private String address;
    public Member(String title, String first_name, String last_name, String url, String contact_form, String facebook, String youtube, String twitter
    , String phoneNumber, String address) {
        this.title = title;
        this.first_name = first_name;
        this.last_name = last_name;
        this.url = url;
        this.contact_form = contact_form;
        this.facebook = facebook;
        this.youtube = youtube;
        this.twitter = twitter;
        this.phoneNumber = phoneNumber;
        this.address = address;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContact_form() {
        return contact_form;
    }

    public void setContact_form(String contact_form) {
        this.contact_form = contact_form;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(title);
        sb.append(",");
        sb.append(this.first_name);
        sb.append(",");
        sb.append(this.last_name);
        sb.append(",");
        if(this.contact_form == null) {
            sb.append(this.url);
        } else {
            sb.append(this.contact_form);
        }
        sb.append(",");
        sb.append(this.facebook);
        sb.append(",");
        sb.append(this.twitter);
        sb.append(",");
        sb.append(this.youtube);
        sb.append(",");
        sb.append(this.phoneNumber);
        sb.append(",");
        sb.append(this.address);

        return sb.toString();
    }
}
