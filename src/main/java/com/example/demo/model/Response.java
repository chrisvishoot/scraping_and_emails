package com.example.demo.model;

import java.util.List;

public class Response {
    private String status;
    private String copyright;
    private List<Result> results;

    @Override
    public String toString() {
        return "Response{" +
                "status='" + status + '\'' +
                ", copyright='" + copyright + '\'' +
                ", results=" + results +
                '}';
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }
}
