package com.example.demo.fileutil;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileUtil {
    public static List<String> listOfEmails() {
        List<String> emails = new ArrayList<>();
        try {
            Scanner scan = new Scanner(new File("listOfEmails.txt"));
            while(scan.hasNext()) {
                String email = scan.nextLine().trim().toLowerCase();
                emails.add(email);

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    return emails;
    }
    public static String readMicrosoftDoc() {
        String fileContents = "";
        try {
            FileInputStream fis = new FileInputStream("input.docx");
            XWPFDocument xdoc = new XWPFDocument(OPCPackage.open(fis));
            XWPFWordExtractor extractor = new XWPFWordExtractor(xdoc);
            fileContents = extractor.getText();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return fileContents;
    }
    public static void WriteToFile(String fileName, String contents) throws IOException {
        FileWriter myWriter = new FileWriter(fileName);
        myWriter.write(contents);
        myWriter.close();
    }
}
