package com.example.demo;

import com.example.demo.api.ParseResponse;
import com.example.demo.api.RestUtil;
import com.example.demo.email.Email;
import com.example.demo.email.SendEmail;
import com.example.demo.fileutil.FileUtil;
import com.example.demo.model.Member;
import com.example.demo.model.Response;
import com.example.demo.model.Result;
import com.squareup.okhttp.ResponseBody;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.Properties;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) throws IOException {
        getCelebrities();
    }

    public static void getCelebrities() {
        RestUtil.getMostPopularCelebrities();

    }



    public static void retrieveSenators(boolean senate) throws IOException {
        String responseBody = null;
        String fileName = "Senators.csv";
        if(senate) {
            responseBody = RestUtil.retrieveSenators();
        } else {
            responseBody = RestUtil.retrieveCongress();
            fileName = "Congress.csv";
        }

        try {
            List<Member> members =  ParseResponse.getResult(responseBody);
            StringBuilder sb = new StringBuilder();
            if(senate) {
                sb.append("Type, Class, Last Name, First Name, Contact Form, Facebook, Twitter, Youtube, Phone Number, Address");
            } else {
                sb.append("Type, Last Name, First Name, Contact Form, Facebook, Twitter, Youtube, Phone Number, Address");
            }
            sb.append(System.lineSeparator());
            for(Member member : members) {
                sb.append(member.toString());
                sb.append(System.lineSeparator());
            }
            FileUtil.WriteToFile(fileName, sb.toString());

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}


