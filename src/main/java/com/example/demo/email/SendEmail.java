package com.example.demo.email;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class SendEmail {
    public static Session getSession(Email email) {
        // Get system properties
        Properties properties = System.getProperties();
        String host = "smtp.gmail.com";

        // Setup mail server
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.auth", "true");

        // Get the Session object.// and pass username and password
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication(email.getUserName(), email.getPassword());

            }

        });

        // Used to debug SMTP issues
        session.setDebug(true);
        return session;
    }
    public void sendEmail(Email email, Session session) {

        // Recipient's email ID needs to be mentioned.
        String to = email.getTo();

        // Sender's email ID needs to be mentioned
        String from = email.getFrom();

        // Assuming you are sending email from through gmails smtp




        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            message.setSubject(email.getSubject());

            // Now set the actual message
            message.setText(email.getContents());


            System.out.println("sending...");
            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }

    }
}
